#include "remove_duplicates.h"

#include <iostream>
#include <set>
#include <string>

void RemoveDuplicates(SearchServer& search_server) {
  std::set<int> duplicate_indexes;
  std::set<std::set<std::string_view, std::less<>>> unique_sets;
  size_t unique_sets_count = 0;

  using std::string_literals::operator""s;

  for (const int id : search_server) {
    std::set<std::string_view, std::less<>> words;
    const auto& word_frequencies = search_server.GetWordFrequencies(id);
    for (const auto& elements : word_frequencies)
      words.insert(elements.first);
    unique_sets.insert(words);
    if (unique_sets.size() > unique_sets_count)
      ++unique_sets_count;
    else {
      std::cout << "Found duplicate document id "s << id << std::endl;
      duplicate_indexes.insert(id);
    }
  }

  for (const int document_id : duplicate_indexes)
    search_server.RemoveDocument(document_id);
}
