#pragma once

#include <deque>
#include <string>
#include <vector>

#include "document.h"
#include "search_server.h"

class RequestQueue {
 public:
  
  explicit RequestQueue(const SearchServer& search_server);

  template <typename DocumentPredicate>
  std::vector<Document> AddFindRequest(const std::string& raw_query, DocumentPredicate document_predicate);
  std::vector<Document> AddFindRequest(const std::string& raw_query, DocumentStatus status);
  std::vector<Document> AddFindRequest(const std::string& raw_query);

  int GetNoResultRequests() const;

 private:
 
  struct QueryResult {
    QueryResult(const bool is_empty, const std::vector<Document>& matched_documents)
        : IsEmpty(is_empty)
        , MatchedDocuments(matched_documents)
    {}
    
    bool IsEmpty;
    std::vector<Document> MatchedDocuments;
  };

  std::deque<QueryResult> requests_;
  const SearchServer& search_server_;
  const static int sec_in_day_ = 1440;
  int seconds_passed_ = 0;
  int no_result_requests_ = 0;

  void UpdateSearchHistory(const std::vector<Document>& request);

};

template <typename DocumentPredicate>
std::vector<Document> RequestQueue::AddFindRequest(const std::string& raw_query, DocumentPredicate document_predicate) {
  std::vector<Document> current_request = search_server_.FindTopDocuments(raw_query, document_predicate);
  UpdateSearchHistory(current_request);
  return current_request;
}
