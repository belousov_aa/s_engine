#include "request_queue.h"

RequestQueue::RequestQueue(const SearchServer& search_server)
    : search_server_(search_server)
{}

std::vector<Document> RequestQueue::AddFindRequest(const std::string& raw_query, DocumentStatus status) {
  std::vector<Document> current_request = search_server_.FindTopDocuments(raw_query, status);
  UpdateSearchHistory(current_request);
  return current_request;
}

std::vector<Document> RequestQueue::AddFindRequest(const std::string& raw_query) {
  std::vector<Document> current_request = search_server_.FindTopDocuments(raw_query);
  UpdateSearchHistory(current_request);
  return current_request;
}

int RequestQueue::GetNoResultRequests() const {
  return no_result_requests_;
}

void RequestQueue::UpdateSearchHistory(const std::vector<Document>& request) {      
  QueryResult current_result(request.empty(), request);
  if (current_result.IsEmpty) {
    ++no_result_requests_;
  }
  requests_.push_back(current_result);

  if (seconds_passed_ < sec_in_day_) {
    ++seconds_passed_;
  } else {
    if (requests_.front().IsEmpty) {
      --no_result_requests_;
    }
    requests_.pop_front();
  }
}
