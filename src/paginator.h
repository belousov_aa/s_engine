#pragma once

#include <iostream>
#include <vector>

template <typename Iterator>
class IteratorRange {
 public:
  explicit IteratorRange(const Iterator page_begin, const Iterator page_end, const size_t page_size)
      : page_begin_(page_begin)
      , page_end_(page_end)
      , page_size_(page_size)
  {}
	
  auto begin() const { 
    return page_begin_;
  } 
	
  auto end() const {
    return page_end_;
  } 
	
  size_t size() const {
    return page_size_;
  }
	
 private:
  Iterator page_begin_;
  Iterator page_end_;
  size_t page_size_;
};

template <typename Iterator>
class Paginator {
 public:
  explicit Paginator(Iterator page_begin, Iterator page_end, const size_t page_size) {
    size_t pages = distance(page_begin, page_end) / page_size;
    if ((distance(page_begin, page_end) % page_size != 0))
      ++pages;
    for (size_t i = 0; i < pages; ++i) {
      Iterator new_begin = page_begin + page_size * i;
      Iterator new_end = (i + 1 == pages)
          ? page_end
          : new_begin + page_size;

      IteratorRange<Iterator> page(new_begin, new_end, page_size);
      book_.push_back(page);
    }
  }
	
  auto begin() const {
    return book_.begin();
  }
	
  auto end() const {
    return book_.end();
  }
	
  size_t size() const {
    return book_.size();
  }
	
 private:
  std::vector<IteratorRange<Iterator>> book_;
};

template <typename Container>
auto Paginate(const Container& c, size_t page_size) {
  return Paginator(begin(c), end(c), page_size);
}

template <typename Iterator>
std::ostream& operator<<(std::ostream& out, const IteratorRange<Iterator>& container) {
  for (auto doc : container){
    using std::string_literals::operator""s;
    out << "{ document_id = "s
        << doc.id 
        << ", relevance = "s
        << doc.relevance
        << ", rating = "s
        << doc.rating
        << " }"s;
  }
  return out;
}
