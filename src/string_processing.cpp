#include "string_processing.h"

std::vector<std::string_view> SplitIntoWords(std::string_view text) {
  std::vector<std::string_view> result;
  size_t length = 0;
  size_t begin = 0;
  for (size_t i = 0; i < text.size(); ++i) {
    if (text[i] == ' ' && length > 0) {
      result.push_back (std::string_view(text.data() + begin, length));
      length = 0;
      begin = i + 1;
    } else {
      ++length;
    }
  }
  if (begin != text.size())
    result.push_back (std::string_view(text.data() + begin, length));
  return result;
}
