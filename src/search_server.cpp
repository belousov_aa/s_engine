#include "search_server.h"

#include <cmath>
#include <iostream>
#include <stdexcept>

#include "log_duration.h"
#include "string_processing.h"

using std::string_literals::operator""s;

void SearchServer::AddDocument(int document_id, std::string_view document,
                               DocumentStatus status, const std::vector<int>& ratings) {
  if (document_id < 0)
    throw std::invalid_argument("Id документа не может быть отрицательным числом"s);
  if (documents_.count(document_id) > 0)
    throw std::invalid_argument("Документ с таким Id уже существует"s);
  const std::vector<std::string_view> words = SplitIntoWordsNoStop(document);
  const double inv_word_count = 1.0 / words.size();
  std::map<std::string_view, double> word_freqs;
  for (std::string_view word : words) {
    if (!IsValidWord(word))
      throw std::invalid_argument("Слово из документа содержит недопустимые символы"s);
    auto saved_word_pair = all_words_.insert(static_cast<std::string>(word));
    word_to_document_freqs_[*saved_word_pair.first][document_id] += inv_word_count;
    word_freqs[*saved_word_pair.first] += inv_word_count;
  }
  documents_.emplace(document_id,
                     DocumentData{ComputeAverageRating(ratings), status, word_freqs});
  document_ids_.insert(document_id);
}

std::set<int>::iterator SearchServer::begin() {
  return document_ids_.begin();
}

int SearchServer::ComputeAverageRating(const std::vector<int>& ratings) {
  if (ratings.empty()) {
    return 0;
  }
  int rating_sum = 0;
  for (const int rating : ratings) {
    rating_sum += rating;
  }
  return rating_sum / static_cast<int>(ratings.size());
}

double SearchServer::ComputeWordInverseDocumentFreq(std::string_view word) const {
  return std::log(GetDocumentCount() * 1.0 / word_to_document_freqs_.at(word).size());
}

std::set<int>::iterator SearchServer::end() {
  return document_ids_.end();
}

std::vector<Document>
SearchServer::FindTopDocuments(std::string_view raw_query,
                               DocumentStatus status) const {
  return FindTopDocuments(std::execution::seq, raw_query,
                          [status](int document_id, DocumentStatus document_status,
                                   int rating) {return document_status == status;});
}

std::vector<Document> SearchServer::FindTopDocuments(std::string_view raw_query) const {
  return FindTopDocuments(std::execution::seq, raw_query, DocumentStatus::ACTUAL);
}

int SearchServer::GetDocumentCount() const {
  return documents_.size();
}

const std::map<std::string_view, double>&
SearchServer::GetWordFrequencies(int document_id) const {
  return (std::find(document_ids_.begin(),
                    document_ids_.end(),
                    document_id) != document_ids_.end())
      ? documents_.at(document_id).word_freqs
      : empty_map_;
}

bool SearchServer::IsStopWord(std::string_view word) const {
  return stop_words_.count(word) > 0;
}

bool SearchServer::IsValidWord(std::string_view word) {
  return std::none_of(word.begin(),
                      word.end(),
                      [](char c) {return c >= '\0' && c < ' ';});
}

std::tuple<std::vector<std::string_view>, DocumentStatus>
SearchServer::MatchDocument(std::string_view raw_query, int document_id) const {
  return MatchDocument(std::execution::seq, raw_query, document_id);
}

std::tuple<std::vector<std::string_view>, DocumentStatus>
SearchServer::MatchDocument(const std::execution::sequenced_policy& policy,
                            std::string_view raw_query,
                            int document_id) const {
  const Query query = ParseQuery(raw_query);
  std::vector<std::string_view> matched_words;
  
  for (std::string_view word : query.minus_words) {
    if (word_to_document_freqs_.find(word) == word_to_document_freqs_.end()) {
      continue;
    }
    if (word_to_document_freqs_.at(word).count(document_id)) {
      return {{}, documents_.at(document_id).status};
    }
  } 
  for (std::string_view word : query.plus_words) {
    if (word_to_document_freqs_.find(word) == word_to_document_freqs_.end()) {
      continue;
    }
    if (word_to_document_freqs_.at(word).count(document_id)) {
      matched_words.push_back(word);
    }
  }
  return make_tuple(matched_words, documents_.at(document_id).status);

}

std::tuple<std::vector<std::string_view>, DocumentStatus>
SearchServer::MatchDocument(const std::execution::parallel_policy& policy,
                            std::string_view raw_query,
                            int document_id) const {
  const Query query = ParseQuery(raw_query, false);
  auto word_check = [&, document_id](std::string_view word) {
    const auto it =  word_to_document_freqs_.find(word);
    return it != word_to_document_freqs_.end()
        && it->second.count(document_id);
  };
 
  if (std::any_of(std::execution::par,
                  query.minus_words.begin(), query.minus_words.end(),
                  word_check)){
    return {{}, documents_.at(document_id).status};
  }
  
  std::vector<std::string_view> matched_words(query.plus_words.size());
  auto word_end = std::copy_if(
      std::execution::par,
      query.plus_words.begin(),query.plus_words.end(),
      matched_words.begin(),
      word_check);
  std::sort(matched_words.begin(), word_end);
  matched_words.erase(std::unique(matched_words.begin(), word_end),
                      matched_words.end());
  
  return std::make_tuple(matched_words, documents_.at(document_id).status);
}

SearchServer::Query SearchServer::ParseQuery(std::string_view text) const {
  return ParseQuery(text, true);
}

SearchServer::Query SearchServer::ParseQuery(std::string_view text,
                                             bool no_duplicates) const {
  Query query;
  for (std::string_view word : SplitIntoWords(text)) {
    const QueryWord query_word = ParseQueryWord(word);
    if (!query_word.is_stop) {
      if (query_word.is_minus) {
        query.minus_words.push_back(query_word.data);
      } else {
        query.plus_words.push_back(query_word.data);
      }
    }
  }

  if (no_duplicates){
    std::sort(query.plus_words.begin(), query.plus_words.end());
    std::sort(query.minus_words.begin(), query.minus_words.end());
    auto it1 = std::unique(query.plus_words.begin(),query.plus_words.end());
    auto it2 = std::unique(query.minus_words.begin(),query.minus_words.end());
    query.plus_words.erase(it1, query.plus_words.end());
    query.minus_words.erase(it2, query.minus_words.end());
  }

  return query;
}

SearchServer::QueryWord SearchServer::ParseQueryWord(std::string_view text) const {
  if (text.empty())
    throw std::invalid_argument("Пустое слово недопустимо"s);
  if (!IsValidWord(text))
    throw std::invalid_argument("Слово содержит недопустимые символы"s);

  bool is_minus = false;
  if (text[0] == '-') {
    is_minus = true;
    text.remove_prefix(1);
    if (text.empty())
      throw std::invalid_argument("Пустое минус-слово недопустимо"s);
    if (text.at(0) == '-')
      throw std::invalid_argument("Минус-слова не должны содержать больше одного минуса"s);
  }
  return QueryWord{ text, is_minus, IsStopWord(text)};
}

void SearchServer::RemoveDocument(int document_id) {
  RemoveDocument(std::execution::seq, document_id);
}

SearchServer::SearchServer(std::string_view stop_words_text)
    : SearchServer(SplitIntoWords(stop_words_text)) {
} 
SearchServer::SearchServer(const std::string& stop_words_text)
    : SearchServer(SplitIntoWords(stop_words_text)) {
} 

std::vector<std::string_view>
SearchServer::SplitIntoWordsNoStop(std::string_view text) const {
  std::vector<std::string_view> words;
  for (std::string_view word : SplitIntoWords(text)) {
    if (!IsStopWord(word)) {
      words.push_back(word);
    }
  }
  return words;
}
