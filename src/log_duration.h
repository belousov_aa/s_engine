#pragma once

#include <chrono>
#include <iostream>

#define PROFILE_CONCAT_INTERNAL(X, Y) X##Y
#define PROFILE_CONCAT(X, Y) PROFILE_CONCAT_INTERNAL(X, Y)
#define UNIQUE_VAR_NAME_PROFILE PROFILE_CONCAT(profileGuard, __LINE__)
#define LOG_DURATION(x) LogDuration UNIQUE_VAR_NAME_PROFILE(x)

#define PROFILE_CONCAT_INTERNAL(X, Y) X##Y
#define PROFILE_CONCAT(X, Y) PROFILE_CONCAT_INTERNAL(X, Y)
#define UNIQUE_VAR_NAME_PROFILE PROFILE_CONCAT(profileGuard, __LINE__)
#define LOG_DURATION_STREAM(x,y) LogDuration UNIQUE_VAR_NAME_PROFILE(x,y)

template<typename StringType>
class LogDuration {
 public:
  
  using Clock = std::chrono::steady_clock;

  LogDuration(const StringType& id, std::ostream& stream_out = std::cerr)
      : id_(id)
      , stream_out_(stream_out)
  {}

  ~LogDuration() {
    using namespace std::chrono;
    using namespace std::literals;

    const auto end_time = Clock::now();
    const auto dur = end_time - start_time_;
    stream_out_ << id_ << ": "s << duration_cast<milliseconds>(dur).count()
                << " ms"s << std::endl;
  }

 private:
  const StringType id_;
  const Clock::time_point start_time_ = Clock::now();
  std::ostream& stream_out_;
};
