#include <execution>
#include <iostream>
#include <random>
#include <string>
#include <string_view>
#include <vector>

#include "log_duration.h"
#include "search_server.h"

using namespace std;

// Функции для демонстрации параллельного поиска

string GenerateWord(mt19937& generator, int max_length) {
  const int length = uniform_int_distribution(1, max_length)(generator);
  string word;
  word.reserve(length);
  for (int i = 0; i < length; ++i) {
    word.push_back(uniform_int_distribution('a', 'z')(generator));
  }
  return word;
}

vector<string> GenerateDictionary(mt19937& generator, int word_count, int max_length) {
  vector<string> words;
  words.reserve(word_count);
  for (int i = 0; i < word_count; ++i) {
    words.push_back(GenerateWord(generator, max_length));
  }
  words.erase(unique(words.begin(), words.end()), words.end());
  return words;
}

string GenerateQuery(mt19937& generator, const vector<string>& dictionary, int word_count, double minus_prob = 0) {
  string query;
  for (int i = 0; i < word_count; ++i) {
    if (!query.empty()) {
      query.push_back(' ');
    }
    if (uniform_real_distribution<>(0, 1)(generator) < minus_prob) {
      query.push_back('-');
    }
    query += dictionary[uniform_int_distribution<int>(0, dictionary.size() - 1)(generator)];
  }
  return query;
}

vector<string> GenerateQueries(mt19937& generator, const vector<string>& dictionary, int query_count, int max_word_count) {
  vector<string> queries;
  queries.reserve(query_count);
  for (int i = 0; i < query_count; ++i) {
    queries.push_back(GenerateQuery(generator, dictionary, max_word_count));
  }
  return queries;
}

template <typename ExecutionPolicy>
void Test(string_view mark, const SearchServer& search_server, const vector<string>& queries, ExecutionPolicy&& policy) {
  LOG_DURATION(mark);
  double total_relevance = 0;
  for (const string_view query : queries) {
    for (const auto& document : search_server.FindTopDocuments(policy, query)) {
      total_relevance += document.relevance;
    }
  }
}


const static vector<string> docs {
  "купить холодильник"s,
  "холодильник со скидкой"s,
  "гоночная машина"s,
  "холодильник недорого в кредит"s,
  "стиральная машина недорого"s,
  "недорогая стиральная машина"s,
  "машина в рассрочку"s
};


void PrintDocument(const Document& document) {
  cout << "{ "s
       << "document_id = "sv << document.id << ", "sv
       << "request = "sv << docs.at(document.id) << ", "sv
       << "relevance = "sv << document.relevance << ", "sv
       << "rating = "sv << document.rating << " }"sv << endl;
}

int main() {
  {
    std::string ignored_words = "и в на с со для"s;
  
    SearchServer search_server(ignored_words);

    search_server.AddDocument(0, docs.at(0), DocumentStatus::ACTUAL, {3, -4});
    search_server.AddDocument(1, docs.at(1), DocumentStatus::ACTUAL, {1, 7, 5 -2});
    search_server.AddDocument(2, docs.at(2), DocumentStatus::BANNED, {2, 7});
    search_server.AddDocument(3, docs.at(3), DocumentStatus::ACTUAL, {6,-1, -4});
    search_server.AddDocument(4, docs.at(4), DocumentStatus::ACTUAL, {1, 5, 9, 4});
    search_server.AddDocument(5, docs.at(5), DocumentStatus::ACTUAL, {3, 3, 8, 4, 8});
    search_server.AddDocument(6, docs.at(6), DocumentStatus::ACTUAL, {6, 8, 7});
  
    cout << "================================================\n"sv;
    cout << "Запросы, cреди которых будет производиться поиск:"sv << '\n';
    for (size_t id = 0; id < docs.size(); ++id)
      cout << "id: "sv << id << ", Запрос: "sv << '"' << docs.at(id) << '"' << '\n';
    cout << "================================================\n\n"sv;

    {
      cout << "Результаты по запросу \"машина в кредит\":\n"sv;
      const auto& matches = search_server.FindTopDocuments("машина в кредит"s);
      for (const Document& document : matches)
        PrintDocument(document);
      cout << '\n';
    }
  
    {
      cout << "Результаты по запросу \"машина в кредит\" среди BANNED-запросов:\n"sv;
      const auto& matches = search_server.FindTopDocuments("машина в кредит"s,
                                                           DocumentStatus::BANNED);
      for (const Document& document : matches)
        PrintDocument(document);
      cout << '\n';
    }
  
    {
      cout << "Результаты по запросу \"машина в кредит -недорогая\":\n"sv;
      const auto& matches = search_server.FindTopDocuments(execution::seq,
                                                           "машина в кредит -недорогая"s);
      for (const Document& document : matches)
        PrintDocument(document);
      cout << '\n';
    }
  
    {
      cout << "Результаты по запросу \"холодильник недорого\":\n"sv;
      const auto& matches = search_server.FindTopDocuments(execution::par,
                                                           "холодильник недорого"s);
      for (const Document& document : matches)
        PrintDocument(document);
      cout << '\n';
    }

  }
  {
    mt19937 generator;
    
    const auto dictionary = GenerateDictionary(generator, 1000, 10);
    const auto documents = GenerateQueries(generator, dictionary, 10'000, 70);
    
    SearchServer search_server(dictionary[0]);
    for (size_t i = 0; i < documents.size(); ++i) {
      search_server.AddDocument(i, documents[i], DocumentStatus::ACTUAL, {1, 2, 3});
    }
    
    const auto queries = GenerateQueries(generator, dictionary, 100, 70);
    
    cout << "Время выполнения последовательного поиска по большому набору данных:\n"sv;
    Test("seq", search_server, queries, std::execution::seq);
    cout << "Время выполнения параллельного поиска по набору тех же данных:\n"sv;
    Test("par", search_server, queries, std::execution::par);
  }
  
  return 0;
}
