#pragma once

#include <algorithm>
#include <cstdlib>
#include <execution>
#include <forward_list>
#include <future>
#include <map>
#include <mutex>
#include <numeric>
#include <random>
#include <string>
#include <vector>

using namespace std::string_literals;

template <typename Key, typename Value>
class ConcurrentMap {
 public:
  static_assert(std::is_integral_v<Key>, "ConcurrentMap supports only integer keys"s);

  struct Bucket {
    std::map<Key, Value> bucket;
    std::mutex bucket_mutex;
  };

  struct Access {
    Value& ref_to_value;
    std::mutex& local_mutex;
  };
  
  explicit ConcurrentMap(size_t bucket_count)
      : all_buckets_(bucket_count)
  {}

  Access operator[](const Key key) {
    const size_t index = BucketIndex(key);
    auto& bucket = all_buckets_[index];
    std::lock_guard lock(bucket.bucket_mutex);
    if (bucket.bucket.count(key) == 0)
      bucket.bucket.insert({key, Value()});
    return Access{bucket.bucket[key], bucket.bucket_mutex};
  }


  std::map<Key, Value> BuildOrdinaryMap() {
    std::map<Key, Value> result;
    for (auto& bucket : all_buckets_) {
      std::lock_guard lock(bucket.bucket_mutex);
      std::map<Key, Value> tmp(bucket.bucket.begin(), bucket.bucket.end());
      result.merge(tmp);
    }
    return result;
  }
  
  void erase(const Key key) noexcept {
    Bucket& bucket = all_buckets_[BucketIndex(key)];
    std::lock_guard lock(bucket.bucket_mutex);
    bucket.bucket.erase(key);
  }
  

 private:
  std::vector<Bucket> all_buckets_;

  size_t BucketIndex(const Key key) const {
    return key % all_buckets_.size();
  }
};
