#pragma once

#include <algorithm>
#include <execution>
#include <map>
#include <numeric>
#include <set>
#include <stdexcept>
#include <string>

#include "concurrent_map.h"
#include "document.h"
#include "string_processing.h"

class SearchServer {
 public:

  template <typename StringContainer> SearchServer(const StringContainer& stop_words);
  explicit SearchServer(std::string_view stop_words_text);
  explicit SearchServer(const std::string& stop_words_text);
   
  void AddDocument(int document_id, std::string_view document,
                   DocumentStatus status, const std::vector<int>& ratings);

  std::set<int>::iterator begin();
  std::set<int>::iterator end();

  template <typename Policy, typename DocumentPredicate>
  std::vector<Document> FindTopDocuments(const Policy& policy,
                                         std::string_view raw_query,
                                         DocumentPredicate document_predicate) const;
  template <typename Policy>
  std::vector<Document> FindTopDocuments(const Policy& policy,
                                         std::string_view raw_query,
                                         DocumentStatus status) const;
  template <typename Policy>
  std::vector<Document> FindTopDocuments(const Policy& policy,
                                         std::string_view raw_query) const;
        
  template <typename DocumentPredicate>
  std::vector<Document> FindTopDocuments(std::string_view raw_query,
                                         DocumentPredicate document_predicate) const;
  std::vector<Document> FindTopDocuments(std::string_view raw_query,
                                         DocumentStatus status) const;
  std::vector<Document> FindTopDocuments(std::string_view raw_query) const;

  int GetDocumentCount() const;

  const std::map<std::string_view, double>& GetWordFrequencies(int document_id) const;
 
  std::tuple<std::vector<std::string_view>, DocumentStatus>
  MatchDocument(std::string_view raw_query, int document_id) const;

  std::tuple<std::vector<std::string_view>, DocumentStatus>
  MatchDocument(const std::execution::sequenced_policy& policy,
                std::string_view raw_query, int document_id) const;

  std::tuple<std::vector<std::string_view>, DocumentStatus>
  MatchDocument(const std::execution::parallel_policy& policy,
                std::string_view raw_query, int document_id) const;

  template<typename ExecutionPolicy>
  void RemoveDocument(ExecutionPolicy&& policy, int document_id);
  void RemoveDocument(int document_id);
   
 private:

  struct DocumentData {
    int rating;
    DocumentStatus status;
    std::map<std::string_view, double> word_freqs;
  };

  struct Query {
    std::vector<std::string_view> plus_words;
    std::vector<std::string_view> minus_words;
  };

  struct QueryWord {
    std::string_view data;
    bool is_minus;
    bool is_stop;
  };

  std::set<std::string> all_words_;
  std::map<int, DocumentData> documents_;
  std::set<int> document_ids_;
  std::map<std::string_view, double> empty_map_;  
  std::set<std::string_view, std::less<>> stop_words_;
  std::map<std::string_view, std::map<int, double>> word_to_document_freqs_;

  static int ComputeAverageRating(const std::vector<int>& ratings);
  double ComputeWordInverseDocumentFreq(std::string_view word) const;

  template <typename DocumentPredicate>
  std::vector<Document> FindAllDocuments(const std::execution::parallel_policy&,
                                         const Query& query,
                                         DocumentPredicate document_predicate) const;
  template <typename DocumentPredicate>
  std::vector<Document> FindAllDocuments(const std::execution::sequenced_policy&,
                                         const Query& query,
                                         DocumentPredicate document_predicate) const;
  template <typename DocumentPredicate>
  std::vector<Document> FindAllDocuments(const Query& query,
                                         DocumentPredicate document_predicate) const;

  bool IsStopWord(std::string_view word) const;
  static bool IsValidWord(std::string_view word);
  Query ParseQuery(std::string_view text) const;
  Query ParseQuery(std::string_view text, bool no_duplicates) const;
  QueryWord ParseQueryWord(std::string_view text) const;
  std::vector<std::string_view> SplitIntoWordsNoStop(std::string_view text) const;  
};



template <typename DocumentPredicate>
std::vector<Document>
SearchServer::FindAllDocuments(const std::execution::sequenced_policy&,
                               const Query& query,
                               DocumentPredicate document_predicate) const {
  std::map<int, double> document_to_relevance;
  for (std::string_view word : query.plus_words) {
    if (word_to_document_freqs_.find(word) == word_to_document_freqs_.end()) {
      continue;
    }
    const double inverse_document_freq = ComputeWordInverseDocumentFreq(word);
    for (const auto [document_id, term_freq] : word_to_document_freqs_.at(word)) {
      const auto& document_data = documents_.at(document_id);
      if (document_predicate(document_id, document_data.status, document_data.rating)) {
        document_to_relevance[document_id] += term_freq * inverse_document_freq;
      }
    }
  }
  for (std::string_view word : query.minus_words) {
    if (word_to_document_freqs_.find(word) == word_to_document_freqs_.end()) {
      continue;
    }
    for (const auto output_pair : word_to_document_freqs_.at(word)) {
      document_to_relevance.erase(output_pair.first);
    }
  }

  std::vector<Document> matched_documents;
  for (const auto [document_id, relevance] : document_to_relevance) {
    matched_documents.push_back({document_id, relevance, documents_.at(document_id).rating});
  }
  return matched_documents;
}

template <typename DocumentPredicate>
std::vector<Document>
SearchServer::FindAllDocuments(const std::execution::parallel_policy&,
                               const Query& query,
                               DocumentPredicate document_predicate) const  {
  
  ConcurrentMap<int, double> document_to_relevance(6);
  
  std::for_each(std::execution::par, query.plus_words.begin(), query.plus_words.end(),
                [&](std::string_view word) {
                  if (word_to_document_freqs_.find(word) != word_to_document_freqs_.end()) {
                    const double inverse_document_freq = ComputeWordInverseDocumentFreq(word);
                    for (const auto [document_id, term_freq] : word_to_document_freqs_.at(word)) {
                      const auto& document_data = documents_.at(document_id);
                      if (document_predicate(document_id, document_data.status, document_data.rating)) {
                        document_to_relevance[document_id].ref_to_value += term_freq * inverse_document_freq;
                      }
                    }
                  }
                });
  
  std::for_each(std::execution::par, query.minus_words.begin(), query.minus_words.end(),
                [&](std::string_view word) {
                  if (word_to_document_freqs_.find(word) != word_to_document_freqs_.end()) {
                    for (const auto output_pair : word_to_document_freqs_.at(word)) {
                      document_to_relevance.erase(output_pair.first);
                    }
                  }
                });
  
  std::vector<Document> matched_documents;

  for (const auto [document_id, relevance] : document_to_relevance.BuildOrdinaryMap()) {
    matched_documents.push_back({document_id, relevance, documents_.at(document_id).rating});
  } 
  return matched_documents;
}

template <typename DocumentPredicate>
std::vector<Document>
SearchServer::FindAllDocuments(const Query& query,
                               DocumentPredicate document_predicate) const {
  return FindAllDocuments(std::execution::seq, query, document_predicate);
}

template <typename Policy, typename DocumentPredicate>
std::vector<Document>
SearchServer::FindTopDocuments(const Policy& policy,
                               std::string_view raw_query,
                               DocumentPredicate document_predicate) const {
  const Query query = ParseQuery(raw_query);
  auto matched_documents = FindAllDocuments(policy, query, document_predicate);
  std::sort(policy, matched_documents.begin(), matched_documents.end(), 
            [](const Document& lhs, const Document& rhs) {
              if (std::abs(lhs.relevance - rhs.relevance) < 1e-6) {
                return lhs.rating > rhs.rating;
              } else {
                return lhs.relevance > rhs.relevance;
              }
            });
  if (matched_documents.size() > MAX_RESULT_DOCUMENT_COUNT) {
    matched_documents.resize(MAX_RESULT_DOCUMENT_COUNT);
  }
  return matched_documents;
}

template <typename Policy>
std::vector<Document>
SearchServer::FindTopDocuments(const Policy& policy,
                               std::string_view raw_query,
                               DocumentStatus status) const {
  return FindTopDocuments(policy, raw_query,
                          [status](int document_id,
                                   DocumentStatus document_status,
                                   int rating)
                          {return document_status == status;});  
}
template <typename Policy>
std::vector<Document>
SearchServer::FindTopDocuments(const Policy& policy,
                               std::string_view raw_query) const {
  return FindTopDocuments(policy, raw_query, DocumentStatus::ACTUAL);
}

template <typename DocumentPredicate>
std::vector<Document>
SearchServer::FindTopDocuments(std::string_view raw_query,
                               DocumentPredicate document_predicate) const {
  return FindTopDocuments(std::execution::seq, raw_query, document_predicate);
}

template<typename ExecutionPolicy>
void SearchServer::RemoveDocument(ExecutionPolicy&& policy, int document_id){
  std::for_each(policy,
                documents_.at(document_id).word_freqs.begin(),
                documents_.at(document_id).word_freqs.end(),
                [&](const auto& element) {
                  word_to_document_freqs_[element.first].erase(document_id);
                });
  documents_.erase(document_id);
  document_ids_.erase(document_id);
}

template <typename StringContainer>
SearchServer::SearchServer(const StringContainer& stop_words){
  using std::string_literals::operator""s;
  std::set<std::string> tmp  = MakeUniqueNonEmptyStrings(stop_words); 
  if (std::any_of(tmp.begin(), tmp.end(), [](const std::string& word) {
    return !IsValidWord(word);}))
    throw std::invalid_argument("Стоп-слова содержат недопустимые символы"s);
  all_words_.insert(tmp.begin(), tmp.end());
  stop_words_.insert(all_words_.begin(), all_words_.end());
}
