#include "process_queries.h"

#include <algorithm>
#include <execution>

std::vector<std::vector<Document>> ProcessQueries(
    const SearchServer& search_server,
    const std::vector<std::string>& queries) {

  std::vector<std::vector<Document>> result(queries.size());
  std::transform(std::execution::par,
                 queries.begin(), queries.end(),
                 result.begin(),
                 [&](const std::string& str) {
                   return search_server.FindTopDocuments(str);});
  return result;
}

std::vector<Document> ProcessQueriesJoined(
    const SearchServer& search_server,
    const std::vector<std::string>& queries) {

  std::vector<std::vector<Document>> found_vector(queries.size());
  std::transform(std::execution::par,
                 queries.begin(), queries.end(),
                 found_vector.begin(),
                 [&](const std::string& str) {
                   return search_server.FindTopDocuments(str);});

  std::vector<Document> result;
  for (std::vector<Document> docs : found_vector) {
    result.insert(result.end(), docs.size(), {});
    std::transform(std::execution::par,
                   docs.begin(), docs.end(),
                   result.end() - docs.size(),
                   [](const Document& doc) {
                     return doc;});
  }
  return result;
}
